# This is the initial attempt to connect to the Ambient Weather API and pull data from Lake NW sensor

from ambient_api.ambientapi import AmbientAPI
import datetime
import os
import asyncio
import logging as logger
import bitdotio
import time
from aioambient import Websocket
from aioambient.errors import WebsocketError
from psycopg2 import sql

macaddress_name = []
db = None


def record_data(data):
    """Write data to database"""
    if data.get('windspeedmph') is None:
        with open('No Connection Record.csv', 'a+') as f:
            f.write('No connection to ' + find_name(data.get('macAddress')) + ' sensor at ' +
                    str(datetime.datetime.fromtimestamp(data.get('dateutc') / 1000)) + '\n')
            f.close()
    else:
        try:
            conn = db.get_connection('Psypoch/data')
            cur = conn.cursor()
            cur.execute(
                sql.SQL("INSERT INTO {table} VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);").format(
                    table=sql.Identifier(find_name(data.get('macAddress')).strip())),
                [data.get('dateutc'),
                 data.get('tempf'), data.get('dewPoint'),
                 data.get('windspeedmph'), data.get('windgustmph'),
                 data.get('winddir'), general_direction(data.get('winddir')),
                 data.get('hourlyrainin'), data.get('humidity'),
                 data.get('solarradiation'), data.get('baromabsin')])
        except Exception as e:
            # handle exception by logging error, wait 2 seconds, and call the function again
            logger.exception('A query error occurred')
            raise e
            time.sleep(2)
            intitial_connection(data)
        cur.close()
        conn.commit()
        conn.close()


def print_goodbye():
    """Print a simple "goodbye" message."""
    print("Client has disconnected from the websocket")


def print_hello():
    """Print a simple "hello" message."""
    print("Client has connected to the websocket")


def find_name(macaddress):
    device_name = [x[1] for x in macaddress_name if x[0] == macaddress]
    return device_name[0]


def general_direction(wind_direction):
    """Get the general direction of the wind based on the """
    if wind_direction >= 0 and wind_direction < 23:
        return 'North'
    elif wind_direction >= 23 and wind_direction < 68:
        return 'Northeast'
    elif wind_direction >= 68 and wind_direction < 113:
        return 'East'
    elif wind_direction >= 113 and wind_direction < 158:
        return 'Southeast'
    elif wind_direction >= 158 and wind_direction < 203:
        return 'South'
    elif wind_direction >= 203 and wind_direction < 248:
        return 'Southwest'
    elif wind_direction >= 248 and wind_direction < 293:
        return 'West'
    elif wind_direction >= 293 and wind_direction < 338:
        return 'Northwest'
    elif wind_direction >= 338 and wind_direction < 360:
        return 'North'


def initial_connection(data):
    """Write out subscription data as it is received."""
    for device in data.get('devices'):
        macaddress_name.append([device.get('macAddress'), device.get('info').get('name')])
        # Upload data to sql
        try:
            conn = db.get_connection('Psypoch/data')
            cur = conn.cursor()
            cur.execute(sql.SQL("CREATE TABLE If Not Exists {table} (date_time bigint PRIMARY KEY, "
                                "temperature float(24), dew_point float(24), wind_speed float(24), wind_gust float(24),"
                                "wind_dir smallint, gen_wind_dir varchar(10), hourly_rain smallint,"
                                "humidity integer, solar_rad float(24), abs_pres float(24))")
                        .format(table=sql.Identifier(device.get('info').get('name').strip())))
            cur.execute(sql.SQL("INSERT INTO {table} VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) On "
                                "Conflict Do Nothing;").format(
                table=sql.Identifier(device.get('info').get('name').strip())), [device.get('lastData').get('dateutc'),
                device.get('lastData').get('tempf'), device.get('lastData').get('dewPoint'),
                device.get('lastData').get('windspeedmph'), device.get('lastData').get('windgustmph'),
                device.get('lastData').get('winddir'), general_direction(device.get('lastData').get('winddir')),
                device.get('lastData').get('hourlyrainin'), device.get('lastData').get('humidity'),
                device.get('lastData').get('solarradiation'), device.get('lastData').get('baromabsin')])
        except Exception as e:
            # handle exception by logging error, wait 2 seconds, and call the function again
            logger.exception('A query error occurred')
            raise e
            time.sleep(2)
            intitial_connection(data)
        cur.close()
        conn.commit()
        conn.close()


async def main() -> None:
    global db
    """Run the websocket example."""
    websocket = Websocket(app_key, api_key)
    websocket.on_connect(print_hello)
    websocket.on_disconnect(print_goodbye)
    websocket.on_subscribed(initial_connection)
    websocket.on_data(record_data)
    db = bitdotio.bitdotio(database_api_key)

    try:
        await websocket.connect()
    except WebsocketError as err:
        print("There was a websocket error: %s", err)
        return

    while True:
        # print("Simulating some other task occurring...")
        await asyncio.sleep(5)


if __name__ == '__main__':
    # initialize variables from environment
    try:
        endpoint = os.environ["AMBIENT_ENDPOINT"]
        rt_endpoint = os.environ["AMBIENT_RT_ENDPOINT"]
        app_key = os.environ["AMBIENT_APPLICATION_KEY"]
        api_key = os.environ["AMBIENT_API_KEY"]
        database_api_key = os.environ['BIT_IO_API_Key']
    except KeyError:
        pass

    # Connect to the Real Time API and pull continuous data
    asyncio.run(main())
